const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var BabelPlugin = require("babel-webpack-plugin");
const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
// importLoader:1 from https://blog.madewithenvy.com/webpack-2-postcss-cssnext-fdcd2fd7d0bd

module.exports = {
    // devtool: 'source-map', // No need for dev tool in production

    module: {
        rules: [{
            test: /\.css$/,
            use: ExtractTextPlugin.extract([
                {
                    loader: 'css-loader',
                    options: { importLoaders: 1 },
                },
                'postcss-loader']
            )
        }, {
            test: /\.sass/,
            use: ExtractTextPlugin.extract([
                {
                    loader: 'css-loader',
                    options: { importLoaders: 1 },
                },
                'postcss-loader',
                {
                    loader: 'sass-loader',
                    options: {
                        data: `@import "${__dirname}/../static/styles/config/_variables.sass";`
                    }
                }]
            )
        }],
    },

    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new CompressionPlugin({
            algorithm: "gzip",
            threshold: 10240,
            minRatio: 0.8
        }),
        new BabelPlugin({
            test: /\.js$/,
            presets: [
                [
                    'env',
                    {
                        exclude: [
                            'transform-regenerator'
                        ],
                        loose: true,
                        modules: false,
                        targets: {
                            browsers: [
                                '>1%'
                            ]
                        },
                        useBuiltIns: true
                    }
                ]
            ],
            sourceMaps: false,
            compact: false
        }),

        new BrotliPlugin({
            asset: '[path].br[query]',
            test: /\.js$|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0.9
        }),
        new ExtractTextPlugin('styles/[name].css'),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            compress: {
                sequences: true,
                booleans: true,
                loops: true,
                unused: true,
                warnings: false,
                drop_console: true,
                unsafe: true,

            }
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
    ]
};
