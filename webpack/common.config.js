const path = require('path');
const autoprefixer = require('autoprefixer');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
var BabelPlugin = require("babel-webpack-plugin");

const TARGET = process.env.npm_lifecycle_event;
var PATHS = {};

switch (TARGET) {
    case 'prod':
        var PATHS = {
            app: path.join(__dirname, '../static'),
            build: path.join(__dirname, '../static_dist')
        };
        var favicon_path = '../static/images/favicon/favicon.ico';
        var index_path = '../static/index.html';

        break;
    case 'dev':
        var PATHS = {
            app: path.join(__dirname, '../src/static'),
            build: path.join(__dirname, '../src/static_dist')
        };
        var index_path = '../src/static/index.html';
        var favicon_path = '../src/static/images/favicon/favicon.ico';
        break;
}

const VENDOR = [
    'babel-polyfill',
    'history',
    'react',
    'react-dom',
    'react-redux',
    'react-router',
    'react-mixin',
    'classnames',
    'redux',
    'react-router-redux',
];

const basePath = path.resolve(__dirname, '../static/');

const common = {
    context: basePath,
    entry: {
        vendor: VENDOR,
        app: PATHS.app
    },
    output: {
        filename: '[name].[hash].js',
        path: PATHS.build,
        publicPath: '/static'
    },
    plugins: [

        // extract all common modules to vendor so we can load multiple apps in one page
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'vendor',
        //     filename: 'vendor.[hash].js'
        // }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, index_path),
            hash: true,
            chunks: ['vendor', 'app'],
            chunksSortMode: 'manual',
            filename: 'index.html',
            inject: 'body',
            favicon: favicon_path
        }),

        new webpack.DefinePlugin({
            'process.env': {NODE_ENV: TARGET === 'dev' ? '"development"' : '"production"'},
            '__DEVELOPMENT__': TARGET === 'dev'
        }),

        new CleanWebpackPlugin([PATHS.build], {
            root: process.cwd()
        }),

    ],
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.sass', '.css'],
        modules: ['node_modules']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    babelrc: false,
                    presets: ['env', 'stage-0', 'react'],
                }
            },
            {
                test: /\.jpe?g$|\.ico$|\.gif$|\.png$/,
                loader: 'file-loader?name=/images/[name].[ext]?[hash]'
            },
            {
                test: /\.woff(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.woff2(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff2'
            },
            {
                test: /\.ttf(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?.*)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]'
            },
            {
                test: /\.otf(\?.*)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]&mimetype=application/font-otf'
            },
            {
                test: /\.svg(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.json(\?.*)?$/,
                loader: 'file-loader?name=/files/[name].[ext]'
            }
        ]
    },
};

switch (TARGET) {
    case 'dev':
        module.exports = merge(require('./dev.config'), common);
        break;
    case 'prod':
        module.exports = merge(require('./prod.config'), common);
        break;
    default:
        console.log('Target configuration not found. Valid targets: "dev" or "prod".');
}
