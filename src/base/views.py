# -*- coding: utf-8 -*-
import logging
import os

from django.conf import settings
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.views.generic import View
from knox.auth import TokenAuthentication
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blog.models import Article

logger = logging.getLogger(__name__)


class IndexView(View):
    """Render main page."""

    def get(self, request):
        """Return html for main application page."""
        if request.user_agent.is_bot or request.user_agent.browser.family == 'Other' or request.user_agent.device.family == 'Spider' \
                or request.user_agent.browser.family == 'YandexBot':
            if 'post' in request.path.split('/') and 'null' != request.path.split('/')[2]:
                slug = request.path.split('/')[2]
                article = Article.objects.get(slug=slug)
                context = {'article': article}
                return TemplateResponse(request, 'article.html', context)

        abspath = open(os.path.join(settings.BASE_DIR, 'static_dist/index.html'), 'r')
        return HttpResponse(content=abspath.read())


class ProtectedDataView(GenericAPIView):
    """Return protected data main page."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """Process GET request and return protected data."""

        data = {
            'data': 'Защищеное сообщение с сервера',
        }

        return Response(data, status=status.HTTP_200_OK)
