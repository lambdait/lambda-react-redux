export HomeView from './Home/index';
export NotFoundView from './NotFound/index';
export ArticleView from './Article/index'
export AboutView from './About/index'
export PostsView from './Posts/index'
