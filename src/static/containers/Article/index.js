import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import * as actionArticle from "../../actions/article";
import {bindActionCreators} from "redux";
import PreloaderIcon, {ICON_TYPE} from 'react-preloader-icon';
import {ArticleHeader} from '../../components/ArticleHeader/index'
import {ArticleContent} from '../../components/ArticleContent/index'
import {ArticleFooter} from '../../components/ArticleFooter/index'
import {Helmet} from "react-helmet";

class ArticleView extends React.Component {

    static propTypes = {
        results: PropTypes.array.isRequired,
        count: PropTypes.number.isRequired,
        next: PropTypes.string,
        previous: PropTypes.string,
        actionArticle: PropTypes.shape({
            fetchArticle: PropTypes.func.isRequired,
        }).isRequired,
    };

    static defaultProps = {
        results: [],
        count: 0,
        next: '',
        previous: ''
    };


    componentDidMount() {
        console.log(this.props);
        //Получаем статью по slug
        this.props.actionArticle.fetchArticle(this.props.slug);
        console.log(this.props);
        // Скорлим до компонента
        window.scroll({
            top: 0,
            left: 0,
        });
    }

    render() {
        return (<div>
                {this.props.posts.activePost.post.title ?
                    <div>
                        <Helmet>
                            <title>{this.props.posts.activePost.post.title}|Lambda</title>
                            <link rel="canonical" href="https://lambda-it.ru"/>
                            <meta name="description" content={this.props.posts.activePost.post.seo_description}/>
                            <meta name="keywords" content={this.props.posts.activePost.post.keywords}/>
                            /* Vk & Facebook */
                            <meta property="og:locale" content="ru_RU"/>
                            <meta property="og:type" content="article"/>
                            <meta property="og:title" content={this.props.posts.activePost.post.title}/>
                            <meta property="og:description"
                                  content={this.props.posts.activePost.post.seo_description}/>
                            <meta property="og:image" content={this.props.posts.activePost.post.images.original}/>
                            <meta property="og:url" content={window.location.href}/>
                            <meta property="og:site_name" content="Lambda"/>
                            /* Twitter */
                            <meta name="twitter:card" content="summary"/>
                            <meta name="twitter:site" content="Lambda"/>
                            <meta name="twitter:title" content={this.props.posts.activePost.post.title}/>
                            <meta name="twitter:description"
                                  content={this.props.posts.activePost.post.seo_description}/>
                            <meta name="twitter:image" content={this.props.posts.activePost.post.images.original}/>
                            /* Google and other */
                            <meta itemprop="name" content={this.props.posts.activePost.post.title}/>
                            <meta itemprop="description" content={this.props.posts.activePost.post.seo_description}/>
                            <meta itemprop="image" content={this.props.posts.activePost.post.images.original}/>
                        </Helmet>
                        <ArticleHeader
                            title={this.props.posts.activePost.post.title}
                            date_create={this.props.posts.activePost.post.date_create}
                            category={this.props.posts.activePost.post.category_name}
                            images={this.props.posts.activePost.post.images}
                            author={this.props.posts.activePost.post.author}
                            time_read={this.props.posts.activePost.post.time_read}
                            header_type={this.props.posts.activePost.post.header_type}
                        />
                        {
                            this.props.posts.activePost.post.content && (this.props.posts.activePost.post.content.map((item, index) => {
                                return <ArticleContent
                                    key={index}
                                    block={item.block}
                                    content={item.content}
                                />
                            }))
                        }
                        <ArticleFooter
                            author={this.props.posts.activePost.post.author}
                            tags={this.props.posts.activePost.post.tags}
                            title={this.props.posts.activePost.post.title}
                            image={this.props.posts.activePost.post.image}
                            description={this.props.posts.activePost.post.short_description}
                        />
                    </div>
                    :
                    <div className='preloader'>
                        <PreloaderIcon
                            type={ICON_TYPE.PUFF}
                            size={60}
                            strokeWidth={8}
                            strokeColor="#4d4d4d"
                            duration={800}
                        />
                    </div>
                }

            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...state.resource,
        slug: ownProps.match.params.slug,
        posts: state.posts,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        actionArticle: bindActionCreators(actionArticle, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView);
export {ArticleView as ArticleViewNotConnected};