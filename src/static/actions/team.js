/**
 * Created by lambada on 09.02.18.
 */
import {checkHttpStatus, parseJSON} from "../utils/index";
import fetch from "isomorphic-fetch";
import {RECEIVE_TEAM_LIST, REQUEST_TEAM_LIST, REQUEST_TEAMEVENTS_LIST, RECEIVE_TEAMEVENTS_LIST} from "../constants/index";
import {SERVER_URL} from "../utils/config";


/*
    НАЧАЛО ПОЛУЧЕНИЕ СПИСКА УЧАСТНИКОВ
 */

/*
 * Запрос на получение списка участников
 */
export function requestTeamList() {
    return {
        type: REQUEST_TEAM_LIST,
    }
}

/*
 * Ответ на получение списка участников
 */
export function receiveTeamList(data) {
    return {
        type: RECEIVE_TEAM_LIST,
        results: data.results,
        count: data.count,
        next: data.next,
        previous: data.previous,
    }
}

/*
 * Загрузка списка участников команды
 */
export function fetchTeam() {
    return dispatch => {
        dispatch(requestTeamList());
        return fetch(`${SERVER_URL}/api/team/member/`)
            .then(checkHttpStatus)
            .then(parseJSON)
            .then((response) => {
                dispatch(receiveTeamList(response));
            })
    }
}

/*
    КОНЕЦ ПОЛУЧЕНИЕ СПИСКА УЧАСТНИКОВ
 */



/*
    НАЧАЛО ПОЛУЧЕНИЯ МЕРОПРИЯТИЙ ГДЕ МЫ УЧАСТВОВАЛИ
 */

/*
 * Запрос на получение списка мероприятий где мы участвовали
 */
export function requestTeamEventsList() {
    return {
        type: REQUEST_TEAMEVENTS_LIST,
    }
}

/*
 * Ответ на получение списка мероприятий где мы участвовали
 */
export function receiveTeamEventsList(data) {
    return {
        type: RECEIVE_TEAMEVENTS_LIST,
        results: data.results,
        count: data.count,
        next: data.next,
        previous: data.previous,
    }
}

/*
 * Загрузка списка  мероприятий где мы участвовали
 */
export function fetchTeamEvents() {
    return dispatch => {
        dispatch(requestTeamEventsList());
        return fetch(`${SERVER_URL}/api/team/events/`)
            .then(checkHttpStatus)
            .then(parseJSON)
            .then((response) => {
                dispatch(receiveTeamEventsList(response));
            })
    }
}

/*
    КОНЕЦ ПОЛУЧЕНИЕ СПИСКА МЕРОПРИЯТИЙ ГДЕ МЫ УЧАСТВОВАЛИ
 */