/**
 * Created by lambada on 09.02.18.
 */
import {checkHttpStatus, parseJSON} from "../utils/index";
import fetch from "isomorphic-fetch";
import {SERVER_URL} from "../utils/config";
import {RECEIVE_ARTICLE, RECEIVE_ARTICLE_LIST, REQUEST_ARTICLE, REQUEST_ARTICLE_LIST,} from "../constants/index";

/*
 * Запрос на получение статьи
 */
export function requestArticle() {
    return {
        type: REQUEST_ARTICLE,
    }
}

/*
 * Ответ на получение статьи
 */
export function receiveArticle(data) {
    return {
        type: RECEIVE_ARTICLE,
        post: data
    }
}


/*
 * Запрос на получение списка статей
 */
export function requestArticleList() {
    return {
        type: REQUEST_ARTICLE_LIST,
    }
}

/*
 * Ответ на получение списка статей
 */
export function receiveArticleList(data) {
    return {
        type: RECEIVE_ARTICLE_LIST,
        results: data.results,
        count: data.count,
        next: data.next,
        previous: data.previous,
    }
}


/*
 * Загрузка всех статей
 */
export function fetchArticlesList(nextUrl) {
    return dispatch => {
        if (nextUrl) {
            dispatch(requestArticleList());
            return fetch(nextUrl)
                .then(checkHttpStatus)
                .then(parseJSON)
                .then((response) => {
                    dispatch(receiveArticleList(response));
                })
        }
        else {
            dispatch(requestArticleList());
            return fetch(`${SERVER_URL}/api/article/`)
                .then(checkHttpStatus)
                .then(parseJSON)
                .then((response) => {
                    dispatch(receiveArticleList(response));
                })
        }

    }
}

/*
 * Загрузка статьи по id
 */
export function fetchArticle(slug) {
    return dispatch => {
        return fetch(`${SERVER_URL}/api/article/${slug}/`)
            .then(checkHttpStatus)
            .then(parseJSON)
            .then((response) => {
                dispatch(receiveArticle(response));
            })
    }
}
