import React from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from "@bem-react/core";
import {Icon} from "../../Icon";
import {ICONS} from "../../Icon/icon_constants";
import './articlecontent_quote.sass'

const article_content = cn('articlecontent');
const article_content_main = cn('articlecontent-main');

export const ArticleContent_quote = (w, props) => {

    return (
        <div className={article_content_main()}>
            <div className={article_content({quote: true})}>
                <div className={article_content("wrap")}>
                    <div className={article_content("placeholder")}>
                        <Icon size={30} icon={ICONS.QUOTE} color={"#1875F0"}/>
                    </div>
                    <div className={article_content("text")} dangerouslySetInnerHTML={{__html: props.content.text}}>
                    </div>
                    <div className={article_content("author")} dangerouslySetInnerHTML={{__html: props.content.author}}>
                    </div>
                </div>
            </div>
        </div>

    )
};


export const contentQuote = withBemMod(
    'asome',
    {block: 'quote'},
    ArticleContent_quote
);