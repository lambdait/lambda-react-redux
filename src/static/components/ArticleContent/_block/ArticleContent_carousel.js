import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from "@bem-react/core";
import {Button} from "../../Button/index";
import {Icon} from "../../Icon";
import {ICONS} from "../../Icon/icon_constants";
import {Image} from "../../Image/index";
import './articlecontent_carousel.sass';
import {Carousel} from './Carousel';

const article_content = cn('articlecontent');
const article_content_main = cn('articlecontent-main');


export const ArticleContent_carousel = (w, props) => {
    function runCarousel() {
        new Carousel({
            "wrap": ".articlecontent-wrap",
            "prev": ".articlecontent-left",
            "next": ".articlecontent-right",
            "touch": true,
            "autoplay": false,
            "autoplayDelay": 3000
        });
    }

    setTimeout(runCarousel, 500);

    return (
        <div className={article_content_main()}>
            <div className={article_content({carousel: true})}>
                <div className={article_content("left")}>
                    <Button color={"white"} type={"circle"}
                            icon={<Icon size={30} icon={ICONS.ARROW_LEFT} color={"#ccc"}/>}/>
                </div>
                <div className={article_content("area")}>
                    <div className={article_content("wrap")}>
                        {props.content && props.content.map((el, index) =>
                            <div key={index} className={article_content("item")}>
                                <Image image={el.url} caption={el.caption} thumbnail={el.url}/>
                            </div>
                        )}
                    </div>
                </div>
                <div className={article_content("right")}>

                    <Button color={"white"} type={"circle"}
                            icon={<Icon size={30} icon={ICONS.ARROW_RIGHT} color={"#ccc"}/>}/>
                </div>
            </div>
        </div>

    )
};


export const contentCarousel = withBemMod(
    'asome',
    {block: 'carousel'},
    ArticleContent_carousel
);