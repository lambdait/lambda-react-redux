import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import './title.sass'

const cls = cn('title');

export class Title extends Component {

    render() {
        return (
            <div className={cls()}>
                <h1 className={cls('text')} style={{color: this.props.color}}>
                    {this.props.text}
                </h1>
                <div className={cls('line')} style={{borderColor: this.props.line_color}}></div>
            </div>
        )
    }
}

