import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import {ICONS} from '../Icon/icon_constants';
import {Icon} from '../Icon/index';
import {Sidebar} from '../Sidebar/index';
import './header.sass'

const header = cn('header');

export class Header extends Component {

    state = {
        open_sidebar: false
    };


    _toogleSidebar() {
        this.setState({open_sidebar: !this.state.open_sidebar});
    }


    render(w, props) {
        let head_menu = [
            {
                text: 'Публикации',
                to: '/'
            }, {
                text: 'О нас',
                to: '/about'
            },
        ];
        return (
            <div className={header()}>
                <div className={header("menu")} onClick={this._toogleSidebar.bind(this)}>
                    <Icon size={30} icon={ICONS.MENU} color={"#d8d8d8"}/>
                </div>
                <div className={header("logo")}>
                    <Icon width={100} height={22} icon={ICONS.LAMBDA} color={"#4D4D4D"}/>
                </div>
                <ul className={header("socials")}>
                    <li className={header("item")}>
                        <a className={header("link")} href="https://www.instagram.com/lambda_mai/">
                            <Icon size={30} icon={ICONS.INSTAGRAM} color={"#ccc"} hoverColor={"#000"}/>
                        </a>
                    </li>
                </ul>
                <Sidebar list={head_menu} toogle={this.state.open_sidebar}/>
            </div>
        );
    }
}

