import React from 'react';
import {cn} from '@bem-react/classname';
import "./feed.sass"
import {Card} from '../Card/index';

const feed = cn('feed');

export const Feed = (w, props) => {
    return (
        <div className={feed('list', {column_4: true})}>
            {

                w.list ? (w.list.map(item => {
                    return <Card key={item.title + item.date_create}
                                 title={item.title}
                                 category={item.category_name}
                                 size={item.size}
                                 date={item.date_create}
                                 image_type={item.image_type}
                                 author={item.author}
                                 time_read={item.time_read}
                                 id={item.id}
                                 images={item.images}
                                 slug={item.slug}
                                 preload={item.preload_image}
                                 thumbnail={item.thumbnail}
                    />
                })) : <div>Loading...</div>

            }
        </div>
    );


};

