import React from 'react';
import {cn} from '@bem-react/classname';
import "./card_flat.sass"
import "./card.sass"
import "./card_fill.sass"

const card = cn('card');

export const Card = (w, props) => {
    console.log(w, props);
    return (
        <div className={card()}>
            <div className={card('head')}>
                <span className={card('tag')}>{w.category}</span>
                <div className={card('thumbnail', {m: true})}>
                    <img src={w.image} alt={w.title}/>
                </div>
            </div>
            <div className={card('body')}>
                <div className={card('title', {m: true})}>
                    <span>{w.title}</span>
                </div>
            </div>
            <div className={card('footer')}>
                <div className={card('date')}><span>Дата</span></div>
                <div className={card('views')}><span>11</span></div>
                <div className={card('auhtor')}><span>Имя</span></div>
            </div>
        </div>
    );
};

