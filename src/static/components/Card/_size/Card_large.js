import React from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from '@bem-react/core'
import {Link} from "react-router-dom";
import './card_large.sass'

const card = cn('card');

const Card_large = (w, props) => {

    return (
        <div className={card({large: true, type: props.image_type})}>
            <div className={card('head')}>
                <span className={card('tag')}
                      style={{'backgroundColor': props.category.color}}>{props.category.name}</span>
                <div className={card('thumbnail')}>
                    <picture>
                        <source srcSet={props.images.png.thumbnail}/>
                        <img src={props.images.webp.thumbnail} alt={props.title}/>
                    </picture>
                </div>
            </div>
            <div className={card('body')}>
                <Link to={`/post/${props.slug}`} className={card('title')}>
                    <span>{props.title}</span>
                </Link>
            </div>
            <div className={card('footer')}>
                <div className={card('date')}>
                    <span>{props.date}</span>
                </div>
                <div className={card('views')}>
                    <span>{props.time_read} чтения</span>
                </div>
                <div className={card('author')}><span>{props.author.name}</span></div>
            </div>
        </div>
    );
};

export const cardLarge = withBemMod(
    'asome',
    {size: 'l'},
    Card_large
);