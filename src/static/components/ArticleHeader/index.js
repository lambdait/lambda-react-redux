import {compose} from '@bem-react/core'

import {ArticleHeader as Base} from './ArticleHeader'
import {headerCentred} from './_header_type/ArticleHeader_centred'
import {headerWhite} from './_header_type/ArticleHeader_white'


export const ArticleHeader = compose(
    headerWhite,
    headerCentred)(Base)
