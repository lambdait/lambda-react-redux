import React from 'react';
import {cn} from '@bem-react/classname';
import {withBemMod} from '@bem-react/core'
import {Icon} from "../../Icon";
import {ICONS} from "../../Icon/icon_constants";
import './articleheader_centred.sass'

const article_header = cn('articleheader');

const ArticleHeader_centred = (w, props) => {
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    let isFirefox = typeof InstallTrigger !== 'undefined';
    let img_path = '';
    // all browser exect firefox chrome, not work with webp
    if (!isChrome && !isFirefox) {
        img_path = props.images.png.compressed;
    } else {
        img_path = props.images.webp.compressed;
    }
    return (
        <div className={article_header({centered: true})}
             style={{backgroundImage: `url(${img_path})`, backgroundPosition: 'center'}}>
            <div className={article_header("content")}>
                <div className={article_header("tools")}>
                </div>

                <div className={article_header("main")}>
                    <span className={article_header("tag")} style={{backgroundColor: `${props.category.color}`}}>
                        {props.category.name}
                    </span>
                    <span className={article_header("date")}>
                            {props.date_create}
                    </span>
                    <h1 className={article_header("title")}>
                        {props.title}
                    </h1>
                    <span className={article_header("line")} style={{borderColor: `${props.category.color}`}}></span>
                </div>
                <div className={article_header("tools")}>
                </div>
                <div className={article_header("info")}>
                    <span className={article_header("item")}>
                        <Icon size={30} icon={ICONS.PROFILE} color={"#ccc"}/>
                        <span className={article_header("value")}>{props.author.name}</span>
                    </span>
                    <span className={article_header("item")}>
                        <Icon size={30} icon={ICONS.SCHEDULE} color={"#ccc"}/>
                        <span className={article_header("value")}>{props.time_read} чтения</span>
                    </span>
                </div>
            </div>
        </div>
    );
};

export const headerCentred = withBemMod(
    'asome',
    {header_type: 'centered'},
    ArticleHeader_centred
);