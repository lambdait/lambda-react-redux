import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import './panel.sass'

const cls = cn('panel');

export class Panel extends Component {

    render() {
        return (
            <div className={cls()}>
                <div className={cls('about')}>
                    <div className={cls('wrapper')}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

