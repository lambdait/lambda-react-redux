import React from 'react'
import {Match, Route} from 'react-router-dom'
import {Header} from '../Header/index';
import {Footer} from '../Footer/index';
import {ICONS} from "../Icon/icon_constants";

export const DefaultLayout = ({children, ...rest}) => {
    const links = [
        {
            key: 'main',
            text: 'Главная',
            to: '/'
        }, {
            key: 'pub',
            text: 'Публикации',
            to: '/'
        }
    ];
    const contacts = {
        phone: '+7 (495) 055-28-96',
        email: 'info@lambda-it.ru'
    };
    const socials = [
        {
            key: 'vk',
            icon: ICONS.VK,
            to: 'https://vk.com/lambdamai'
        },

        {
            key: 'inst',
            icon: ICONS.INSTAGRAM,
            to: 'https://www.instagram.com/lambda_mai/'
        },
    ];

    return (
        <React.Fragment>
            <Header/>
            {children}
            <Footer menu={links} socials={socials} contacts={contacts}/>
        </React.Fragment>
    )
};

export const DefaultRoute = ({component: Component, ...rest}) => {
    return (
        <Route {...rest} render={matchProps => (
            <DefaultLayout>
                <Component {...matchProps} />
            </DefaultLayout>
        )}/>
    )
};