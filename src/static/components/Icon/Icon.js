import React, {Component} from 'react';
import {cn} from '@bem-react/classname';
import "./icon.sass"

const icon = cn('icon');

export const Icon = (w, props) => {
    if (w.height && w.width) {
        return (
            <svg className={icon()}
                 width={`${w.width}px`}
                 height={`${w.height}px`}
                 viewBox={`0 0 ${w.width} ${w.height}`}>
                <path className={icon("path")} fill={w.color} d={w.icon}/>
            </svg>
        );
    }
    return (
        <svg className={icon()}
             width={`${w.size}px`}
             height={`${w.size}px`}
             viewBox={`0 0 30 30`}>
            <path className={icon("path")} fill={w.color} d={w.icon}/>
        </svg>
    );
};

