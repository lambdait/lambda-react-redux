import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import postsReducer from "./article";
import teamsReducer from "./team";

export default combineReducers({
    posts: postsReducer, // Хранилише статей
    team: teamsReducer, // Хранилише команды
    routing: routerReducer, // Хранилише роутинга
});
