/**
 * Created by mr9bit on 09.02.18.
 */
import {RECEIVE_ARTICLE, RECEIVE_ARTICLE_LIST, REQUEST_ARTICLE, REQUEST_ARTICLE_LIST} from "../constants";

const initialState = {
    postsList: {
        results: [],
        count: 0,
        next: '',
        previous: '',
        isFetching: false,
    },
    activePost: {
        post: '',
        isFetching: false
    },
};
export default function postsReducer(state = initialState, action) {
    switch (action.type) {
        case REQUEST_ARTICLE_LIST:
            return Object.assign({}, state, {
                postsList: {
                    isFetching: true,
                    results: state.postsList.results,
                }
            });
        case REQUEST_ARTICLE:
            return Object.assign({}, state, {
                activePost: {
                    post: state.activePost.post,
                    isFetching: true,
                }
            });

        case RECEIVE_ARTICLE:
            return Object.assign({}, state, {
                activePost: {
                    post: action.post,
                    isFetching: false
                }
            });
        case RECEIVE_ARTICLE_LIST: {
            /*
             * Сравнение двух списков статей если есть отличие, то соеденить их.
             */
            if (action.results.toString() !== state.postsList.results.toString()) {
                return Object.assign({}, state, {
                    postsList: {
                        results: state.postsList.results.concat(action.results),
                        count: action.count,
                        next: action.next,
                        previous: action.previous,
                        isFetching: false,
                    }
                });
            }
        }


        default:
            return state
    }
}