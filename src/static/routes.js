import React from 'react';
import {Switch} from 'react-router-dom'
import {AboutView, ArticleView, HomeView, PostsView} from './containers';
import {DefaultRoute} from './components/Layout/Default'
import {TransparentRoute} from './components/Layout/Transparent'


export default (
    <Switch>
        <DefaultRoute key={"home"} exact path="/" component={HomeView}/>
        <DefaultRoute key={"allpost"} path='/posts' component={PostsView}/>
        <DefaultRoute key={"article"} path='/post/:slug' component={ArticleView}/>
        <TransparentRoute key={"about"} path='/about' component={AboutView}/>
    </Switch>
);