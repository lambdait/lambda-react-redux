# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2019-02-10 15:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('blog', '0005_auto_20190203_0645'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='compressed_image',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Сжатое изображение'),
        ),
        migrations.AlterField(
            model_name='article',
            name='thumbnail',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Сжатое изображение предпосмотр'),
        ),
    ]
