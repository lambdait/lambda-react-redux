# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2019-04-25 21:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20190425_1529'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='teamevent',
            options={'verbose_name': 'Меропритяие', 'verbose_name_plural': 'Меропритяия'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'ordering': ['order'], 'verbose_name': 'Участник', 'verbose_name_plural': 'Участники'},
        ),
        migrations.AddField(
            model_name='user',
            name='order',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
