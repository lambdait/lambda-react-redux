import uuid
from datetime import timedelta

import unidecode
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.template import defaultfilters
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blog.signals import compress_img


class MyUserManager(BaseUserManager):
    def _create_user(self, email, password, first_name, last_name, is_staff, is_superuser, **extra_fields):
        """
        Create and save an User with the given email, password, name and phone number.

        :param email: string
        :param password: string
        :param first_name: string
        :param last_name: string
        :param is_staff: boolean
        :param is_superuser: boolean
        :param extra_fields:
        :return: User
        """
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(email=email,
                          first_name=first_name,
                          last_name=last_name,
                          is_staff=is_staff,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, first_name, last_name, password, **extra_fields):
        """
        Create and save an User with the given email, password and name.

        :param email: string
        :param first_name: string
        :param last_name: string
        :param password: string
        :param extra_fields:
        :return: User
        """

        return self._create_user(email, password, first_name, last_name, is_staff=False, is_superuser=False,
                                 **extra_fields)

    def create_superuser(self, email, first_name='', last_name='', password=None, **extra_fields):
        """
        Create a super user.

        :param email: string
        :param first_name: string
        :param last_name: string
        :param password: string
        :param extra_fields:
        :return: User
        """
        return self._create_user(email, password, first_name, last_name, is_staff=True, is_superuser=True,
                                 **extra_fields)


class User(AbstractBaseUser):
    """
    Model that represents an user.

    To be active, the user must register and confirm his email.
    """

    GENDER_MALE = 'М'
    GENDER_FEMALE = 'Ж'
    GENDER_CHOICES = (
        (GENDER_MALE, 'Мужской'),
        (GENDER_FEMALE, 'Женский')
    )

    def get_file_path(self, filename):
        """
            Функция для получения пути для сохранения файла
        :param filename: название файла
        :return: местоположение файла
        """
        return '{0}/{1}/{2}'.format('team/member',
                                    defaultfilters.slugify(unidecode.unidecode(self.first_name + '_' + self.last_name)),
                                    filename)

    # we want primary key to be called id so need to ignore pytlint
    # В чем фича данного поля?
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)  # pylint: disable=invalid-name
    first_name = models.CharField(_('Имя'), max_length=50)
    last_name = models.CharField(_('Фамилия'), max_length=50)
    email = models.EmailField(_('Почта'), unique=True)
    position = models.CharField(_('Должность'), max_length=300, default='Участник')
    order = models.PositiveIntegerField(default=1, blank=False, null=False)

    # Изображения
    image = models.ImageField(_("Фотография"), default='', upload_to=get_file_path)
    thumbnail_png = models.ImageField(_("Миниатюра PNG"), default='', null=True, blank=True)
    thumbnail_webp = models.ImageField(_("Миниатюра WEBP"), default='', null=True, blank=True)
    thumbnail_512_png = models.ImageField(verbose_name="Миниатюра 512 PNG", blank=True, null=True)
    thumbnail_512_webp = models.ImageField(verbose_name="Миниатюра 512 WEBP", blank=True, null=True)

    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default=GENDER_MALE)

    confirmed_email = models.BooleanField(default=False)

    is_staff = models.BooleanField(_('staff status'), default=False)
    is_superuser = models.BooleanField(_('superuser status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)

    date_joined = models.DateTimeField(_('Дата регистрации'), auto_now_add=True)
    date_updated = models.DateTimeField(_('date updated'), auto_now=True)

    activation_key = models.UUIDField(unique=True, default=uuid.uuid4)  # email

    USERNAME_FIELD = 'email'

    objects = MyUserManager()

    class Meta:
        verbose_name = "Участник"
        verbose_name_plural = "Участники"
        ordering = ['-order']

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser

    def __str__(self):
        """
        Unicode representation for an user model.

        :return: string
        """
        return ' '.join([self.first_name, self.last_name])

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.

        :return: string
        """
        return "{0} {1}".format(self.first_name, self.last_name)

    def get_short_name(self):
        """
        Return the first_name.

        :return: string
        """
        return self.first_name

    def activation_expired(self):
        """
        Check if user's activation has expired.

        :return: boolean
        """
        return self.date_joined + timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS) < timezone.now()

    def confirm_email(self):
        """
        Confirm email.

        :return: boolean
        """
        if not self.activation_expired() and not self.confirmed_email:
            self.confirmed_email = True
            self.save()
            return True
        return False

    def save(self, *args, **kwargs):
        obj = None
        created = False
        try:
            # Пытаемся получить прошлое состояние
            obj = User.objects.get(id=self.id)
        except Exception as e:
            # Если ничего не получается, то просто создаем объект
            created = True
        # Сохроняем
        instance = super(User, self).save(*args, **kwargs)
        instance = self
        if created or obj.image != instance.image:
            if created or obj.image != self.image:
                thumbnail_webp, thumbnail_png = compress_img((64, 64), instance.image.path, instance.image.url,
                                                             'thumbnail')
                main_webp, main_png = compress_img((512, 512), instance.image.path, instance.image.url,
                                                   'main')
                instance.thumbnail_webp = thumbnail_webp
                instance.thumbnail_512_png = main_png
                instance.thumbnail_512_webp = main_webp
                instance.thumbnail_png = thumbnail_png
                instance.save()
        return instance






SOCIAL_CHOICES = (
    ('github', "GitHub"),
    ('vk', "Вконтакте"),
    ('instagram', "Instagram"),
    ('twitter', "Twitter"),
)


class SocialNetwork(models.Model):
    name = models.CharField(_("Название"), max_length=300, choices=SOCIAL_CHOICES)
    url = models.URLField(_("Ссылка"), max_length=200)
    user = models.ForeignKey(User, default=0, related_name='social_networks')

    def __str__(self):
        return self.url


class TeamEvent(models.Model):
    def get_file_path(self, filename):
        """
            Функция для получения пути для сохранения файла
        :param filename: название файла
        :return: местоположение файла
        """
        return '{0}/{1}/{2}'.format('team/event',
                                    defaultfilters.slugify(unidecode.unidecode(self.name)),
                                    filename)

    name = models.CharField(_("Название"), max_length=500)
    url = models.URLField(_("Ссылка"), max_length=200)
    image = models.ImageField(_("Фотография"), default='', upload_to=get_file_path)
    thumbnail_png = models.ImageField(_("Миниатюра PNG"), default='', null=True, blank=True)
    thumbnail_webp = models.ImageField(_("Миниатюра WEBP"), default='', null=True, blank=True)
    order = models.PositiveIntegerField(default=1, blank=False, null=False)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = "Мероприятие"
        verbose_name_plural = "Мероприятия"
        ordering = ['-order']



    def save(self, *args, **kwargs):
        obj = None
        created = False
        try:
            # Пытаемся получить прошлое состояние
            obj = TeamEvent.objects.get(id=self.id)
        except Exception as e:
            # Если ничего не получается, то просто создаем объект
            created = True
        # Сохроняем
        instance = super(TeamEvent, self).save(*args, **kwargs)
        instance = self
        if created or obj.image != instance.image:
            if created or obj.image != self.image:
                thumbnail_webp, thumbnail_png = compress_img((256, 256), instance.image.path, instance.image.url,
                                                             'thumbnail')
                instance.thumbnail_webp = thumbnail_webp
                instance.thumbnail_png = thumbnail_png
                instance.save()
        return instance
